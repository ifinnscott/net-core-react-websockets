import React from 'react';
import PropTypes from 'prop-types';
import { isFunction } from 'underscore';

export default function socketConnect(Target, options = {}) {
  class SocketConnect extends React.Component {
    static contextTypes = {
      socket: PropTypes.object,
      registerSocketEventListener: PropTypes.func
    };

    constructor(props, context) {
      super(props, context);
      this.propagateEvents = this.propagateEvents.bind(this);
    }

    componentWillMount() {
      const { registerSocketEventListener } = this.context;
      registerSocketEventListener(this.propagateEvents);
    }

    propagateEvents(type, params = []) {
      if (options.hasOwnProperty(type) && isFunction(options[type])) {
        options[type].apply(this, [].concat(params, {...this.props, ...this.context.socket}));
      }
    }

    render() {
      const { props, context: { socket } } = this;
      return <Target {...props} {...socket} />
    }
  }

  return SocketConnect;
}
