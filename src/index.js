import SocketProvider from './socketProvider';
import socketConnect from './socketConnector';

module.exports = {
  SocketProvider,
  socketConnect
};
