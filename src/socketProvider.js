import React from 'react';
import PropTypes from 'prop-types';
import { w3cwebsocket as webSocket } from 'websocket';
import { isFunction, has, extend, each } from 'underscore';

const readyState = {
  connecting: 0,
  open: 1,
  closing: 2,
  closed: 3
};

const defaultOptions = {
  host: undefined,
  port: 80,
  path: 'ws'
};

class WebSocketProvider extends React.Component {
  static childContextTypes = {
    socket: PropTypes.object,
    registerSocketEventListener: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      connected: false,
      connecting: false
    };

    this.events = {};
    this.socket = false;
    this.eventListeners = [];

    this.addSocketEvent = this.addSocketEvent.bind(this);
    this.emitSocketEvent = this.emitSocketEvent.bind(this);
    this.getSocketUrl = this.getSocketUrl.bind(this);
    this.registerSocketEventListener = this.registerSocketEventListener.bind(this);

    this.options = Object.assign({}, defaultOptions, props.options);
  }

  componentWillMount() {
    // componentWillMount is executed on the server as well as client
    // Ensure checkSocketConnection is not executed on the server
    if (typeof (self) === 'object')
      this.checkSocketConnection();
  }

  componentDidMount() {
    this.initialiseSocketEvents();
  }

  componentWillUnmount() {
    this.disconnect();
  }

  initialiseSocketEvents() {
    if (!this.socket)
      return false;

    const socket = this.socket;

    socket.onopen = () => {
      if (!this.state.connecting && !this.state.connected) {
        this.setState({ connected: true });
      }

      this.propagateEvents('onOpen');
    };

    socket.onmessage = event => {
      this.onSocketMessage(event);
    };

    socket.onclose = () => {
      this.setState({ connected: false });

      this.propagateEvents('onClose');
    }

    socket.onerror = e => {
      this.setState({ connected: false });

      this.propagateEvents('onError', e);
      this.checkSocketConnection();
    };

    return false;
  }

  checkSocketConnection() {
    const { connected, connecting } = this.state;

    if (!connected && !connecting) {
      this.connect();
    }

    if (connected && !connecting) {
      this.disconnect();
    }
  }

  connect() {
    if (this.socket)
      return false;

    const socket = webSocket(this.getSocketUrl());
    const socketIsConnected = Boolean(socket && socket.readyState === readyState.open);

    if (socketIsConnected) {
      this.setState({ connected: true });
    }

    this.socket = socket;

    return false;
  }

  disconnect() {
    if (this.socket) {
      this.socket.close();
      this.socket = false;
    }
  }

  onSocketMessage(e) {
    try {
      const data = JSON.parse(e.data);

      this.propagateEvents('onMessage', data);

      if (data.hasOwnProperty('type') && this.events.hasOwnProperty(data.type)) {
        this.events[data.type].call(this, data.contents);
      }
    } catch (err) {
      // Ignore
    }
  }

  addSocketEvent(event, callback) {
    if (!has(this.events, event) && isFunction(callback)) {
      const newEvent = { [event]: callback };
      this.events = extend(this.events, newEvent);
    }
  }

  emitSocketEvent(event, data) {
    if (this.socket) {
      const jsonString = JSON.stringify({
        type: event,
        contents: data
      });

      this.socket.send(jsonString);
    }
  }

  registerSocketEventListener(eventListener) {
    this.eventListeners.push(eventListener);
  }

  propagateEvents(type, data) {
    const events = this.eventListeners;

    if (events && events.length > 0) {
      each(events, (event) => {
        if (isFunction(event)) {
          event(type, data);
        }
      })
    }
  }

  getChildContext() {
    return {
      socket: {
        addSocketEvent: this.addSocketEvent,
        emitSocketEvent: this.emitSocketEvent,
        socket: this.socket
      },
      registerSocketEventListener: this.registerSocketEventListener
    };
  }

  getSocketUrl() {
    if (!this.options.host)
      this.options.host = location.hostname;

    if (!this.options.port)
      this.options.port = location.port || 80;

    return `${location.protocol === 'http:' ? 'ws' : 'wss'}://${this.options.host}:${this.options.port}/${this.options.path}`;
  }

  render() {
    return React.Children.only(this.props.children);
  }
}

export default WebSocketProvider;
